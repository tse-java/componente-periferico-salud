package uy.viruscontrol.cps.util;

import uy.viruscontrol.cps.dto.DTOMedic;

import java.util.Calendar;
import java.util.Date;

public class Util {
    public static int rejectRatio = 70;

    public static Date getRandomDate() {
        Date randomDate = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(randomDate);
        c.add(Calendar.DATE, (int) (Math.random() * 2));
        c.add(Calendar.HOUR, (int) (Math.random() * 2));
        c.add(Calendar.MINUTE, (int) (Math.random() * 45)+15);
        return c.getTime();
    }

    public static DTOMedic getRandomMedic(){
        int num = (int) (Math.random() * 10);
        return Persistence.getInstance().getMedics().get(num);
    }
}
