package uy.viruscontrol.cps.util;

import org.jeasy.random.EasyRandom;
import org.jeasy.random.EasyRandomParameters;
import org.jeasy.random.FieldPredicates;
import org.jeasy.random.randomizers.FirstNameRandomizer;
import org.jeasy.random.randomizers.range.IntegerRangeRandomizer;
import org.jeasy.random.randomizers.range.LongRangeRandomizer;
import uy.viruscontrol.cps.dto.DTOMedic;

import java.util.List;
import java.util.stream.Collectors;

public class Persistence {

    private final List<DTOMedic> medics;

    private static Persistence instance;

    private Persistence() {
        EasyRandom generator = new EasyRandom(new EasyRandomParameters()
                .randomize(FieldPredicates.named("name"), new FirstNameRandomizer())
                .randomize(FieldPredicates.named("documentNumber"), new IntegerRangeRandomizer(1000000, 7000000))
        );
        this.medics = generator.objects(DTOMedic.class, 10).collect(Collectors.toList());
    }


    public static Persistence getInstance() {
        if(instance == null){
            instance = new Persistence();
        }
        return instance;
    }

    public List<DTOMedic> getMedics() {
        return medics;
    }
}
