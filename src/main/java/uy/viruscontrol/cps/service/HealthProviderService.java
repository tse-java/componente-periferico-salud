package uy.viruscontrol.cps.service;


import uy.viruscontrol.cps.dto.DTOAppointment;
import uy.viruscontrol.cps.exception.HealthProviderException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlElement;

@WebService
public interface HealthProviderService {
    @WebMethod(operationName = "requestMedic")
    DTOAppointment scheduleAppointment(@WebParam(name = "ci") @XmlElement(required = true) String ci) throws HealthProviderException;
    @WebMethod(operationName = "configure")
    void rejectRatio(@WebParam(name = "rejectRatio") @XmlElement(required = true) Integer rejectRatio) throws HealthProviderException;
    @WebMethod(operationName = "ping")
    String ping();
}

