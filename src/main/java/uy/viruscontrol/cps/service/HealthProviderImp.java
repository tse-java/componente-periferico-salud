package uy.viruscontrol.cps.service;

import uy.viruscontrol.cps.dto.DTOAppointment;
import uy.viruscontrol.cps.exception.HealthProviderException;
import uy.viruscontrol.cps.util.Util;

import javax.jws.WebService;
import java.util.logging.Logger;

@WebService(endpointInterface = "uy.viruscontrol.cps.service.HealthProviderService")
public class HealthProviderImp implements HealthProviderService {
    private static final Logger LOG = Logger.getLogger(HealthProviderImp.class.getName());

    @Override
    public DTOAppointment scheduleAppointment(String ci) throws HealthProviderException {
        LOG.info("Executing operation scheduleAppointment");
        LOG.info(ci);
        int num = (int) (Math.random() * 100) + 1;
        if (num <= Util.rejectRatio) {
            throw new HealthProviderException("We can't accept request at this moment. Try again later.");
        }
        DTOAppointment result = new DTOAppointment();
        result.setMedic(Util.getRandomMedic());
        result.setDate(Util.getRandomDate());
        return result;
    }

    @Override
    public void rejectRatio(Integer rejectRatio) throws HealthProviderException {
        LOG.info("Executing operation rejectRatio");
        LOG.info(rejectRatio.toString());
        if(rejectRatio > 100 || rejectRatio < 0){
            throw new HealthProviderException("Reject ratio must be between 0 and 100.");
        }
        Util.rejectRatio = rejectRatio;
    }

    @Override
    public String ping() {
        return "Pong";
    }
}
