package uy.viruscontrol.cps.exception;

public class HealthProviderException extends Exception{
    public HealthProviderException(String s) {
        super(s);
    }
}
