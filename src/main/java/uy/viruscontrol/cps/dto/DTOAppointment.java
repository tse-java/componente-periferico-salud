package uy.viruscontrol.cps.dto;


import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;

@XmlRootElement
public class DTOAppointment implements Serializable {
    private DTOMedic medic;
    private Date date;

    public DTOAppointment() {
    }

    public DTOMedic getMedic() {
        return medic;
    }

    public void setMedic(DTOMedic medic) {
        this.medic = medic;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
